package com.company;

import java.util.regex.*;

public class Kata {
    public static boolean validPhoneNumber(String phoneNumber) {
        // TODO: Return whether phoneNumber is in the proper form

        Pattern pattern = Pattern.compile("[(][0-9][0-9][0-9][)]\\s[0-9][0-9][0-9][-][0-9][0-9][0-9][0-9]");
        Matcher matcher = pattern.matcher(phoneNumber);

        return matcher.matches();
    }

}
